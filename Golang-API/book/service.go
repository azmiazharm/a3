package book

type Service interface {
	FindAll() ([]Book, error)
	FindByID(ID int) (Book, error)
	CreateBook(bookRequest BookRequest) (Book, error)
	UpdateBook(ID int, bookRequest BookRequest) (Book, error)
	DeleteBook(ID int) (Book, error)
}

type service struct {
	repository Repository
}

func NewService(repository Repository) *service {
	return &service{repository}
}

func (s *service) FindAll() ([]Book, error) {
	books, err := s.repository.FindAll()
	return books, err
	// return s.repository.FindAll()
}

func (s *service) FindByID(ID int) (Book, error) {
	book, err := s.repository.FindByID(ID)
	return book, err
	// return s.repository.FindByID(ID)
}

func (s *service) CreateBook(bookRequest BookRequest) (Book, error) {
	price, err := bookRequest.Price.Int64()
	rating, err := bookRequest.Rating.Int64()
	discount, err := bookRequest.Discount.Int64()

	book := Book{
		Title:       bookRequest.Title,
		Price:       int(price),
		Description: bookRequest.Description,
		Rating:      int(rating),
		Discount:    int(discount),
	}

	newBook, err := s.repository.CreateBook(book)
	return newBook, err
}

func (s *service) UpdateBook(ID int, bookRequest BookRequest) (Book, error) {
	book, err := s.repository.FindByID(ID)

	price, err := bookRequest.Price.Int64()
	rating, err := bookRequest.Rating.Int64()
	discount, err := bookRequest.Discount.Int64()

	book.Title = bookRequest.Title
	book.Price = int(price)
	book.Description = bookRequest.Description
	book.Rating = int(rating)
	book.Discount = int(discount)

	newBook, err := s.repository.UpdateBook(book)
	return newBook, err
	// return s.repository.FindByID(ID)
}

func (s *service) DeleteBook(ID int) (Book, error) {
	book, err := s.repository.FindByID(ID)

	newBook, err := s.repository.DeleteBook(book)
	return newBook, err
	// return s.repository.FindByID(ID)
}
