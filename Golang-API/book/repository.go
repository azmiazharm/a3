package book

import (
	"fmt"

	"gorm.io/gorm"
)

type Repository interface {
	FindAll() ([]Book, error)
	FindByID(ID int) (Book, error)
	CreateBook(book Book) (Book, error)
	UpdateBook(book Book) (Book, error)
	DeleteBook(book Book) (Book, error)
}

type repository struct {
	db *gorm.DB
}

func NewRepository(db *gorm.DB) *repository {
	return &repository{db}
}

func (r *repository) FindAll() ([]Book, error) {
	var books []Book

	err := r.db.Debug().Find(&books).Error
	if err != nil {
		fmt.Println(err)
	}

	return books, err
}

func (r *repository) FindByID(ID int) (Book, error) {
	var book Book

	err := r.db.Debug().Find(&book, ID).Error
	if err != nil {
		fmt.Println(err)
	}

	return book, err
}

func (r *repository) CreateBook(book Book) (Book, error) {

	err := r.db.Debug().Create(&book).Error
	if err != nil {
		fmt.Println(err)
	}

	return book, err
}

func (r *repository) UpdateBook(book Book) (Book, error) {

	err := r.db.Save(&book).Error
	if err != nil {
		fmt.Println(err)
	}

	return book, err
}

func (r *repository) DeleteBook(book Book) (Book, error) {

	err := r.db.Delete(&book).Error
	if err != nil {
		fmt.Println(err)
	}

	return book, err
}
