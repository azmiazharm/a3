package book

type BookResponse struct {
	ID          int    `json:"id"`
	Title       string `json:"title"`
	Description string `json:"description"`
	Discount    int    `json:"discount"`
	Price       int    `json:"price"`
	Rating      int    `json:"rating"`
}
