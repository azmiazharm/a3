package book

import "time"

type Book struct {
	ID          int
	Title       string
	Description string
	Discount    int
	Price       int
	Rating      int
	CreatedAt   time.Time
	UpdateAt    time.Time
}
