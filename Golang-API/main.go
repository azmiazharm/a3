package main

import (
	"fmt"
	"log"
	"pustaka-api/book"
	"pustaka-api/handler"

	"github.com/gin-gonic/gin"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func main() {
	// dsn := "root:root@tcp(127.0.0.1:3306)/pustaka-api?charset=utf8mb4&parseTime=True&loc=Local"
	// db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})

	dsn := "host=localhost user=postgres password=a dbname=pustaka-api port=5432 sslmode=disable TimeZone=Asia/Jakarta"
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatal("DB Connection error")
	}

	db.AutoMigrate(&book.Book{})
	fmt.Println("database konek sukses")

	bookRepository := book.NewRepository(db)
	bookService := book.NewService(bookRepository)
	bookHandler := handler.NewBookHandler(bookService)

	//crud test

	// create
	// book := book.Book{}
	// book.Title = "Atomic Habit"
	// book.Price = 100000
	// book.Rating = 4
	// book.Description = "Buku self development yang paling dicari orang"
	// // book.UpdateAt = time.Now()
	// // book.CreatedAt = time.Now()

	// err = db.Create(&book).Error
	// if err != nil {
	// 	fmt.Println(err)
	// }

	// read
	// var books []book.Book

	// err = db.Debug().Where("rating = ?", 4).Find(&books).Error
	// if err != nil {
	// 	fmt.Println(err)
	// }

	// for _, book := range books {
	// 	fmt.Println("Title :", book.Title)
	// 	fmt.Println("Book Object :", book)
	// }

	// update
	// var book book.Book
	// err = db.Debug().Where("id = ?", 1).First(&book).Error
	// if err != nil {
	// 	fmt.Println(err)
	// }

	// book.Title = "Man with Tiger Lily"
	// err = db.Save(&book).Error
	// if err != nil {
	// 	fmt.Println(err)
	// }

	//delete
	// var book book.Book
	// err = db.Debug().Where("id = ?", 2).First(&book).Error
	// if err != nil {
	// 	fmt.Println(err)
	// }

	// err = db.Delete(&book).Error
	// if err != nil {
	// 	fmt.Println(err)
	// }

	router := gin.Default()

	v1 := router.Group("/v1")
	v1.GET("/books", bookHandler.GetBooksHandler)
	v1.GET("/books/:id", bookHandler.GetBookHandler)
	v1.POST("/books", bookHandler.CreateBookHandler)
	v1.PUT("/books/:id", bookHandler.UpdateBookHandler)
	v1.DELETE("/books/:id", bookHandler.DeleteBookHandler)

	// v1.GET("/", bookHandler.RootHandler)
	// v1.GET("/greeting", bookHandler.HelloHandler)
	// v1.GET("/book/:id/:title", bookHandler.BooksHandler)
	// v1.GET("/query", bookHandler.QueryHandler)
	// v1.POST("/books", bookHandler.PostBookHandler)

	router.Run(":8080")
}

// handler -> service ->repo -> db
