package controllers

import (
	"context"
	"encoding/json"
	"fmt"
	"golang-mongo/models"
	"net/http"

	"github.com/julienschmidt/httprouter"
	"go.mongodb.org/mongo-driver/mongo"
	"gopkg.in/mgo.v2/bson"
)

type UserController struct {
	session *mongo.Client
}

func NewUserController(s *mongo.Client) *UserController {
	return &UserController{s}
}

func (uc UserController) GetUser(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	id := p.ByName("id")

	if !bson.IsObjectIdHex(id) {
		w.WriteHeader(http.StatusNotFound)
	}

	ctx := context.Background()

	oid := bson.ObjectIdHex(id)

	u := models.User{}

	err := uc.session.Database("privy-golang").Collection("users").FindOne(ctx, bson.M{"_id": oid}).Decode(u)
	if err != nil {
		w.WriteHeader(404)
		return
	}

	uj, err := json.Marshal(u)
	if err != nil {
		fmt.Println(err)
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "%s\n", uj)
}

func (uc UserController) CreateUser(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	u := models.User{}

	json.NewDecoder(r.Body).Decode(&u)

	ctx := context.Background()

	u.Id = bson.NewObjectId()

	uc.session.Database("privy-golang").Collection("users").InsertOne(ctx, u)

	uj, err := json.Marshal(u)
	if err != nil {
		fmt.Println(err)
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	fmt.Fprintf(w, "%s\n", uj)
}

func (uc UserController) DeleteUser(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	id := p.ByName("id")

	if !bson.IsObjectIdHex(id) {
		w.WriteHeader(404)
	}

	ctx := context.Background()

	oid := bson.ObjectIdHex(id)

	u := models.User{}

	err := uc.session.Database("privy-golang").Collection("users").FindOneAndDelete(ctx, bson.M{"_id": oid}).Decode(u)
	if err != nil {
		w.WriteHeader(404)
	}

	w.WriteHeader(200)
	fmt.Fprint(w, "deleted user", oid, "\n")
}
