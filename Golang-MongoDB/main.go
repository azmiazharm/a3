package main

import (
	"context"
	"golang-mongo/controllers"
	"net/http"

	"github.com/julienschmidt/httprouter"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func main() {
	r := httprouter.New()
	uc := controllers.NewUserController(getSession())

	r.GET("/user/:id", uc.GetUser)
	r.POST("/user", uc.CreateUser)
	r.DELETE("/user/:id", uc.DeleteUser)

	http.ListenAndServe("localhost:9000", r)
}

func getSession() *mongo.Client {
	// s, err := mgo.Dial("mongodb://127.0.0.1:27017")
	// if err != nil {
	// 	panic(err)
	// }

	// s, err := mongo.NewClient(options.Client().ApplyURI("mongodb+srv://azmiazharm:<passnya>@cluster0.fdjjk.mongodb.net/?retryWrites=true&w=majority"))
	s, err := mongo.NewClient(options.Client().ApplyURI("mongodb+srv://azmiazharm:azmiazharm76@cluster0.fdjjk.mongodb.net/?retryWrites=true&w=majority"))

	if err != nil {
		panic(err)
	}

	ctx := context.Background()
	err = s.Connect(ctx)

	if err != nil {
		panic(err)
	}

	// defer s.Disconnect(ctx)

	return s
}
